﻿using Desklend.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend
{
    public class DesklendDBContext:IdentityDbContext
    {
        public DesklendDBContext(DbContextOptions<DesklendDBContext> options):base(options)
        {

        }
        public DbSet<Company> Company { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Office> Office { get; set; }
        public DbSet<Reservation> Reservation { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Desk> Desk { get; set; }
    }
}
